import {createStore, Store} from 'redux';

export const
    SET_NAVIGATION = 'SET_NAVIGATION';

export interface SetNavigationEvent {
    navigation: string;
}

interface Action {
    type: string;
    setNavigation: SetNavigationEvent
}

export interface State {
    setNavigation: SetNavigationEvent
}

let reducer = ((state: State, action: Action): State => {
    switch (action.type) {
        case SET_NAVIGATION: {
            return {
                ...state, setNavigation: action.setNavigation
            }
        }
        default:
            return state;
    }
});

export function createGlobalStore(): Store<State> {
    return createStore(reducer);
}