// GCS doesn't offer an apache style directory listing on /,
// and I don't want to figure out the GCS javascript API
// or proxy through some backend service, so copypasta

export let ImagesByCategory: Map<string, Array<string>> = new Map([
    ["Bedroom #1", [
        "IMG_3388_medium.JPG",
        "IMG_3389_medium.JPG",
        "IMG_3390_medium.JPG",
        "IMG_3393_medium.JPG"
    ]],
    ["Bedroom #2", [
        "IMG_3375_medium.JPG",
        "IMG_3376_medium.JPG",
        "IMG_3377_medium.JPG",
    ]],
    ["Dining Room", [
        "IMG_3310_medium.JPG",
        "IMG_3318_medium.JPG",
        "IMG_3324_medium.JPG",
    ]],
    ["Exterior", [
        "IMG_3475_medium.JPG",
        "IMG_3478_medium.JPG",
        "IMG_3479_medium.JPG",
        "IMG_3488_medium.JPG",
        "IMG_3513_medium.JPG",
        "IMG_3519_medium.JPG",
        "IMG_3562_medium.JPG",
        "IMG_3563_medium.JPG",
        "IMG_3564_medium.JPG",
    ]],
    ["Garage Bath", [
        "IMG_3454_medium.JPG",
        "IMG_3456_medium.JPG",
        "IMG_3459_medium.JPG",
    ]],
    ["Guest Bath", [
        "IMG_3383_medium.JPG",
        "IMG_3384_medium.JPG",
    ]],
    ["Kitchen", [
        "IMG_3296_medium.JPG",
        "IMG_3303_medium.JPG",
        "IMG_3306_medium.JPG",
        "IMG_3315_medium.JPG",
        "IMG_3325_medium.JPG",
        "IMG_3326_medium.JPG",
        "IMG_3331_medium.JPG",
    ]],
    ["Living Room", [
        "IMG_3354_medium.JPG",
        "IMG_3356_medium.JPG",
        "IMG_3359_medium.JPG",
    ]],
    ["Master Bath", [
        "IMG_3342_medium.JPG",
        "IMG_3344_medium.JPG",
        "IMG_3345_medium.JPG",
    ]],
    ["Master Bedroom", [
        "IMG_3364_medium.JPG",
        "IMG_3402_medium.JPG",
        "IMG_3404_medium.JPG",
        "IMG_3406_medium.JPG",
        "IMG_3412_medium.JPG",
    ]],
    ["Office Room #1", [
        "IMG_3543_medium.JPG",
    ]],
    ["Office Room #2", [
        "IMG_3529_medium.JPG",
        "IMG_3536_medium.JPG",
    ]],
    ["Property", [
        "IMG_3498_medium.JPG",
        "IMG_3500_medium.JPG",
        "IMG_3501_medium.JPG",
        "IMG_3502_medium.JPG",
        "IMG_3507_medium.JPG",
        "IMG_3509_medium.JPG",
        "IMG_3548_medium.JPG",
        "IMG_3551_medium.JPG",
        "IMG_3555_medium.JPG",
    ]],
    ["Sunroom", [
        "IMG_3433_medium.JPG",
        "IMG_3434_medium.JPG",
        "IMG_3435_medium.JPG",
        "IMG_3436_medium.JPG",
        "IMG_3437_medium.JPG",
    ]],
    ["TV Room", [
        "IMG_3568_medium.JPG",
        "IMG_3570_medium.JPG",
        "IMG_3571_medium.JPG",
    ]],
    ["Utility Room", [
        "IMG_3414_medium.JPG",
        "IMG_3415_medium.JPG",
        "IMG_3418_medium.JPG",
    ]],
]);