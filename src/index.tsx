import * as React from "react";
import * as ReactDOM from "react-dom";

import { Viewer } from "./components/Viewer";
import {Navigator} from "./components/Navigator";
import {createGlobalStore, State} from "./utils/GlobalStore";
import {Store} from "react-redux";

let store = createGlobalStore();

// Reused everywhere
export interface GenericProps {
    store: Store<State>;
}

ReactDOM.render(
    <div className={"wrapper"}>
        <Navigator store={store} />
        <Viewer store={store} />
    </div>,
    document.getElementById("root")
);