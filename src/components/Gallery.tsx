import * as LightBox from '../../node_modules/react-image-lightbox/dist/main.js';

import * as React from "react";
import {createGlobalStore} from "../utils/GlobalStore";
import {GenericProps} from "../index";
import {Unsubscribe} from "redux";

interface GalleryState {
    index: number;
    isOpen: boolean;
}

interface GalleryProps {
    photos: Array<string>;
}

export class Gallery extends React.Component<GalleryProps, GalleryState> {

    constructor(props: GalleryProps) {
        super(props);
        this.state = {
            index: 0,
            isOpen: true
        }
    }

    render() {
        if (this.state.isOpen) {
            return (
                <LightBox
                    mainSrc={this.props.photos[this.state.index]}
                    nextSrc={this.props.photos[(this.state.index + 1) % this.props.photos.length]}
                    prevSrc={this.props.photos[(this.state.index + this.props.photos.length - 1) % this.props.photos.length]}
                    onCloseRequest={() => this.setState({isOpen: false})}
                    onMovePrevRequest={() => this.setState({
                        index: (this.state.index + this.props.photos.length - 1) % this.props.photos.length,
                    })}
                    onMoveNextRequest={() => this.setState({
                        index: (this.state.index + 1) % this.props.photos.length,
                    })}
                />
            )
        } else {
            return <div />;
        }
    }
}