import * as React from "react";
import {createGlobalStore} from "../utils/GlobalStore";
import {GenericProps} from "../index";
import {Unsubscribe} from "redux";
import {Gallery} from "./Gallery";
import {ImagesByCategory} from "../utils/Images";

interface ViewerState {
    navigation: string | null;
}

export class Viewer extends React.Component<GenericProps, ViewerState> {
    private unsubscribe: Unsubscribe;

    constructor(props: GenericProps) {
        super(props);

        this.unsubscribe = this.props.store.subscribe(() => this.onStoreTrigger());
        this.state = {
            navigation: null
        };
    }

    render() {

        let photoUrls: Array<string> = [];
        if (this.state.navigation !== null) {
            // Build urls
            let photos = ImagesByCategory.get(this.state.navigation);
            photos.forEach((v) => {
                photoUrls.push(`assets/${encodeURIComponent(this.state.navigation)}/${encodeURIComponent(v)}`);
            });

        }

        return <div className={"viewer"}>
            <div className="showcase">
                <img style={{width: "100%"}} src="assets/Exterior/IMG_3475_medium.JPG" />
            </div>
            <p>
            Beebe 2550 sf 3 to 5 bedroom, 3 bath house and 1800 sf garage on 4 acres.
            <br /><br />
            For sale by original owner.  Easy access to 67/167 freeway (1.5 mi.)  House built in 1995; garage in 2006.  1725 sf main building with 3 bedrooms and 2 baths.  825 sf adjacent building with its own heat and air, 20’ x 20’ family room, and two 10’ x 20’ offices or bedrooms.  30’ x 60’ garage attached to adjacent building with 5 garage bays, bathroom, and storage room with additional washer & dryer connections.
            <br /><br />
            Kitchen remodel in 2012 with custom cabinets, granite countertops, and granite flooring.  New roof in 2009.  Located within city limits on dead-end street with property agreement/covenant.  All electric with city trash and sewer, and option for county or city water.
            <br /><br />
            Features include: fencing on 3 sides, seamless gutters, tankless water heater, open floor plan, laminate flooring, walk-in master bedroom closet, quartz windowsills, non-smoker, current termite contract, and vinyl siding with plywood and Tyvek backing for entire perimeter.  City ordinance allows 1 horse (or cow) per grazing acre.
            <br /><br />
            Shown by appointment.  Asking price $268,000.  Contact Beth at <a href="mailto:114JamieLane@gmail.com?Subject=Untitled" target="_top">114JamieLane@gmail.com</a>.  Please no rent, lease, trade, or owner finance inquiries.
            </p>
            {
                this.state.navigation === null ? null :
                    <Gallery key={this.state.navigation} photos={photoUrls}/>
            }
        </div>
    }

    private onStoreTrigger() {
        let nextState = this.props.store.getState();
        this.setState({
            navigation: nextState.setNavigation.navigation
        });
    }
}