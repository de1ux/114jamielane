import * as React from "react";
import {ImagesByCategory} from "../utils/Images"
import {Store} from "react-redux";
import {SET_NAVIGATION, State} from "../utils/GlobalStore"
import {GenericProps} from "../index";

interface NavigatorState {
    locations: Array<String>;
}

export class Navigator extends React.Component<GenericProps, NavigatorState> {
    constructor(props: GenericProps) {
        super(props);

        let keys: Array<string> = [];
        ImagesByCategory.forEach((v, k) => {
            keys.push(k);
        });
        this.state = {
            locations: keys
        };
    }

    render() {
        return (
            <div className={"navigator"}>
                <p style={{paddingLeft: "40px"}}>Photos</p>
                <ul>
                    {this.state.locations.map(
                        el => <li><a href="#" onClick={(e) => this.handleNavClick(e)}>{el}</a></li>
                    )}
                    <li><a href={`assets/${encodeURIComponent("Building Layout.pdf")}`}>Building Plans</a></li>
                </ul>
            </div>
        );
    }

    private handleNavClick(e: any) {
        this.props.store.dispatch({
            type: SET_NAVIGATION,
            setNavigation: {
                navigation: e.target.innerText
            }
        })
    }
}